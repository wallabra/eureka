//------------------------------------------------------------------------
//  ZETABOT PATH NODE MANIPULATION AND OPERATIONS
//------------------------------------------------------------------------
//
//  Eureka DOOM Editor
//
//  Copyright (C) 2001-2016 Andrew Apted
//  Copyright (C) 1997-2003 Andr� Majorel et al
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 2
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//------------------------------------------------------------------------
//
//  Based on Yadex which incorporated code from DEU 5.21 that was put
//  in the public domain in 1994 by Rapha�l Quinet and Brendon Wyber.
//
//------------------------------------------------------------------------

#include "main.h"
#include "bsp.h"

#include "zb_glue.hpp"
#include "e_objects.h"
#include "ui_window.h"
#include "e_pathnodes.hpp"



extern build_result_e BuildAndGetSubsectors(std::vector<ajbsp::subsec_t*> &subsecs);

void CMD_PN_SpinPathNodes(void)
{
	int degrees = atoi(EXEC_Param[0]);

	if (! degrees)
		degrees = +45;

	selection_c list;
	selection_iterator_c it;

	if (! GetCurrentObjects(&list))
	{
		Beep("No path nodes to spin");
		return;
	}

	BA_Begin();

	for (list.begin(&it) ; !it.at_end() ; ++it)
	{
		ZB_PathNode *PN = *(PathNodes.begin() + (*it));

		BA_ChangePN(*it, ZB_PathNode::F_ANGLE, calc_new_angle(PN->getAngle(), degrees));
	}

	BA_MessageForSel("spun", &list);
	BA_End();
}

// Places some basic path nodes.
void CMD_PN_PlopBasic(void) {
	unsigned long i;

	selection_c list;
	list.change_type(OBJ_PATHNODE);

	BA_Begin();

	// build nodes to get the subsectors
	std::vector<ajbsp::subsec_t*> subsecs;

	build_result_e ret = BuildAndGetSubsectors(subsecs);

	// plop nodes in the center of subsectors
	for (std::vector<ajbsp::subsec_t*>::iterator it = subsecs.begin(); it != subsecs.end(); it++) {
		ajbsp::subsec_t *sub = *it;

		int ni = BA_New(OBJ_PATHNODE);
		ZB_PathNode &nod = *PathNodes[ni];

		BA_ChangePN(ni, ZB_PathNode::F_X, sub->mid_x);
		BA_ChangePN(ni, ZB_PathNode::F_Y, sub->mid_y);

		SnapNodeToFloor(ni, nod);
		
		list.set(ni);
	}

	BA_MessageForSel("plopped", &list);
	BA_End();
}

// Sets the type of a node.
void CMD_PN_RotateNavType(void) {
	int degrees = atoi(EXEC_Param[0]);

	if (! degrees)
		degrees = +45;

	selection_c list;
	selection_iterator_c it;

	if (! GetCurrentObjects(&list))
	{
		Beep("No path nodes whose navigation type to rotate.");
		return;
	}

	BA_Begin();

	for (list.begin(&it) ; !it.at_end() ; ++it)
	{
		ZB_PathNode *PN = *(PathNodes.begin() + (*it));

		PN->setNavigationType((ZB_NavigationType) ((PN->getType() + 1) % NTCOUNT));
	}

	BA_MessageForSel("rotated the navigation type for", &list);
	BA_End();
}

void CMD_PN_AimAtMouse(void) {
	selection_c list;
	selection_iterator_c it;

	if (! GetCurrentObjects(&list))
	{
		Beep("No path nodes to aim");
		return;
	}

	int raw_x, raw_y;

	Fl::get_mouse(raw_x, raw_y);

	raw_x -= main_win->x_root();
	raw_y -= main_win->y_root();

	int x = main_win->canvas->MAPX(raw_x);
	int y = main_win->canvas->MAPY(raw_y);

	BA_Begin();

	for (list.begin(&it) ; !it.at_end() ; ++it)
	{
		ZB_PathNode *PN = *(PathNodes.begin() + (*it));

		int ang = (int)(atan2(y - PN->getY(), x - PN->getX()) * 180 / M_PI);
		BA_ChangePN(*it, ZB_PathNode::F_ANGLE, ang);
	}

	BA_MessageForSel("aimed", &list);
	BA_End();
}