//------------------------------------------------------------------------
//  ZETABOT PATH NODE ROUTINES AND DEFINITIONS
//------------------------------------------------------------------------
//
//  Eureka DOOM Editor
//
//  Copyright (C) 2001-2016 Andrew Apted
//  Copyright (C) 1997-2003 Andr� Majorel et al
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 2
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//------------------------------------------------------------------------
//
//  Based on Yadex which incorporated code from DEU 5.21 that was put
//  in the public domain in 1994 by Rapha�l Quinet and Brendon Wyber.
//
//------------------------------------------------------------------------

#include "zb_node.hpp"
#include "e_main.h"
#include "e_basis.h"



void ZB_ClearNodeList();
Fl_Color ZB_GetNodeColor(ZB_PathNode *node);
Fl_Color ZB_GetNodeTextColor(ZB_PathNode *node);
std::string NodeTypeName(ZB_NavigationType nt);
void SnapNodeToFloor(int nodeID, ZB_PathNode &nod);