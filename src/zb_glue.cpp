#include "zb_node.hpp"
#include "main.h"

#include "ui_window.h"
#include "im_color.h"
#include "e_hover.h"
#include "e_basis.h"



void ZB_ClearNodeList() {
    PathNodes.clear();
    
    if (Level_name && Level_name[0]) {
        _PathNodes.mapName = Level_name;
    }
}

Fl_Color ZB_GetNodeColor(ZB_PathNode *node) {
    ZB_NavigationType nt = node->getType();

    switch (nt) {
        case NT_NORMAL: {
            return CYAN;
        }

        case NT_USE: {
            return GREEN;
        }

        case NT_SLOW: {
            return BLUE;
        }

        case NT_CROUCH: {
            return MAGENTA;
        }

        case NT_JUMP: {
            return YELLOW;
        }

        case NT_AVOID: {
            return RED;
        }

        case NT_SHOOT: {
            return FL_DARK_RED;
        }

        case NT_RESPAWN: {
            return FL_DARK_GREEN;
        }

        default: {
            return FL_BLACK;
        }
    }
}

Fl_Color ZB_GetNodeTextColor(ZB_PathNode *node) {
    ZB_NavigationType nt = node->getType();

    switch (nt) {
        case NT_SHOOT: {
            return WHITE;
        }

        case NT_RESPAWN: {
            return WHITE;
        }

        default: {
            if (ZB_GetNodeColor(node) == FL_BLACK)
                return WHITE;

            else
                return BLACK;
        }
    }
}

std::string NodeTypeName(ZB_NavigationType nt) {
    switch (nt) {
        case NT_NORMAL:     return "Normal";
        case NT_USE:        return "Use";
        case NT_SLOW:       return "Slow";
        case NT_CROUCH:     return "Crouch";
        case NT_JUMP:       return "Jump";
        case NT_AVOID:      return "Avoid";
        case NT_SHOOT:      return "Shoot";
        case NT_RESPAWN:    return "Respawn";
        default:            return "";
    }
}

void SnapNodeToFloor(int nodeID, ZB_PathNode &nod) {
    Objid sid;
    GetNearObject(sid, OBJ_SECTORS, nod.getX(), nod.getY());

    if (sid.is_nil())
        BA_ChangePN(nodeID, ZB_PathNode::F_Z, 0);

    else
        BA_ChangePN(nodeID, ZB_PathNode::F_Z, Sectors[sid.num]->floorh);
}