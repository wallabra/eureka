//------------------------------------------------------------------------
//  PATH NODE PANEL
//------------------------------------------------------------------------
//
//  Eureka DOOM Editor
//
//  Copyright (C) 2007-2018 Andrew Apted
//  Copyright (C)      2015 Ioan Chera
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 2
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//------------------------------------------------------------------------


#pragma once

#include "zb_node.hpp"
#include "ui_nombre.h"
#include "hdr_fltk.h"


class Sticker;


class UI_PathNodeBox : public Fl_Group
{
private:
	int obj;
	int count;

	UI_Nombre *which;

	Fl_Choice     *navType;

	Fl_Int_Input    *angle;
	Fl_Button       *ang_buts[8];

	Fl_Int_Input    *pos_x;
	Fl_Int_Input    *pos_y;
	Fl_Int_Input    *pos_z;

public:
	UI_PathNodeBox(int X, int Y, int W, int H, const char *label = NULL);
	virtual ~UI_PathNodeBox();

public:
	void SetObj(int _index, int _count);

	int GetObj() const { return obj; }

	// call this if the thing was externally changed.
	// -1 means "all fields"
	void UpdateField(int field = -1);

	void UpdateTotal();

	// see ui_window.h for description of these two methods
	bool ClipboardOp(char op);
	void BrowsedItem(char kind, int number, const char *name, int e_state);

	void UpdateGameInfo();

private:
	void SetNavType(ZB_NavigationType new_type);

private:
	static void       x_callback(Fl_Widget *w, void *data);
	static void       y_callback(Fl_Widget *w, void *data);
	static void       z_callback(Fl_Widget *w, void *data);
	static void    type_callback(Fl_Widget *w, void *data);

	static void  angle_callback(Fl_Widget *w, void *data);
	static void button_callback(Fl_Widget *w, void *data);
};