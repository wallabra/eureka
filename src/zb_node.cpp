#include <sstream>
#include <string>
#include <iterator>
#include <cassert>
#include <cstring>
#include <vector>

#include "zb_node.hpp"

#include "sys_type.h"
#include "objid.h"
#include "m_select.h"
#include "e_basis.h"
#include "main.h"



ZB_PathNode::ZB_PathNode() {
    nodeType = NT_NORMAL;
    x = y = z = angle = 0;
}

ZB_PathNode::ZB_PathNode(int x, int y, int z, ZB_NavigationType nodeType, unsigned short angle) {
    this->x = x;
    this->y = y;
    this->z = z;
    this->nodeType = nodeType;
    this->angle = angle;
}

ZB_PathNode::ZB_PathNode(std::string commaData) {
    Parse(commaData);
}

void ZB_PathNode::Parse(std::string commaData) {
    std::istringstream iss(commaData);
    std::string token;
    unsigned char pos = 0;

    while (std::getline(iss, token, ',')) {
        switch (pos) {
            case 0:
                this->x = std::stoi(token);
                break;
            
            case 1:
                this->y = std::stoi(token);
                break;

            case 2:
                this->z = std::stoi(token);
                break;

            case 3:
                this->nodeType = ZB_NavigationType(std::stoi(token));
                break;

            case 4:
                this->angle = std::stoi(token);
                break;

            default:
                BugError("Excess info in pathnode text data (ZBNODES)!");
                return;
        }

        pos++;
    }
}

std::string ZB_PathNode::getCommaData() {
    std::stringstream ss;
    ss << x << "," << y << "," << z << "," << nodeType << "," << angle;

    return ss.str();
}

void ZB_PathNode::offset(int xOff, int yOff) {
    x += xOff;
    y += yOff;
}

void ZB_PathNode::move(int x, int y) {
    this->x = x;
    this->y = y;
}

void ZB_PathNode::setHeight(int z) {
    this->z = z;
}

void ZB_PathNode::turnTo(unsigned short angle) {
    this->angle = angle;
}

void ZB_PathNode::setNavigationType(ZB_NavigationType newNT) {
    nodeType = newNT;
}



ZB_MapNodes::ZB_MapNodes() {
    mapName = "MAP01";
    assert(mapNodes.empty());
}

ZB_MapNodes::ZB_MapNodes(std::string mapName) {
    this->mapName = mapName;
    assert(mapNodes.empty());
}

#include <iostream>

void ZB_ParseNodes(std::string data) {
    if (data.find("::NONE") == 0)
        return;

    short pos = data.find("::");

    assert(pos != -1);

    std::string mn = data.substr(0, pos);
    std::string nodes = data.substr(pos + 2);

    _PathNodes.mapName = mn;

    std::stringstream ss(nodes);
    std::string node;

    while (std::getline(ss, node, ':')) {
        ZB_PathNode *nod = new ZB_PathNode;
        PathNodes.push_back(nod);

        nod->Parse(node);
    }
}

std::string ZB_MapNodes::getData() {
    if (mapNodes.size() == 0)
        return "::NONE";

    std::stringstream ss;
    ss << mapName << "::";

    for (std::vector<ZB_PathNode*>::iterator it = mapNodes.begin(); it != mapNodes.end(); ++it) {
        ss << (*it)->getCommaData() << ":";
    }

    std::string res = ss.str();
    res.pop_back();
    
    return res;
}

int ZB_PathNode::getX() {
    return x;
}

int ZB_PathNode::getY() {
    return y;
}

int ZB_PathNode::getZ() {
    return z;
}

ZB_NavigationType ZB_PathNode::getType() {
    return nodeType;
}

unsigned short ZB_PathNode::getAngle() {
    return angle;
}