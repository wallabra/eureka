#include <sstream>

#include "main.h"

#include "ui_window.h"
#include "zb_node.hpp"
#include "ui_nombre.h"
#include "ui_prefs.h"
#include "ui_pathnodes.hpp"
#include "e_basis.h"



extern const char * arrow_0_xpm[];
extern const char * arrow_45_xpm[];
extern const char * arrow_90_xpm[];
extern const char * arrow_135_xpm[];
extern const char * arrow_180_xpm[];
extern const char * arrow_225_xpm[];
extern const char * arrow_270_xpm[];
extern const char * arrow_315_xpm[];

extern const char ** arrow_pixmaps[8];



UI_PathNodeBox::UI_PathNodeBox(int X, int Y, int W, int H, const char *label) :
    Fl_Group(X, Y, W, H, label),
    obj(-1), count(0)
{
	box(FL_FLAT_BOX);

	X += 6;
	Y += 6;

	W -= 12;
	H -= 10;


	which = new UI_Nombre(X+6, Y, W-12, 28, "Path Node");

	Y = Y + which->h() + 4;


	navType = new Fl_Choice(X+70, Y, 70, 24, "Type: ");
	navType->align(FL_ALIGN_LEFT);
	navType->when(FL_WHEN_RELEASE | FL_WHEN_ENTER_KEY);
	navType->type(FL_INT_INPUT);

	// add navigation types
	for (ZB_NavigationType nt = ZB_NavigationType(0); nt < NTCOUNT; nt = ZB_NavigationType(nt + 1)) {
		navType->add(NodeTypeName(nt).c_str(), 0, type_callback, this, 0);
	}

	Y = Y + navType->h() + 4;

	pos_x = new Fl_Int_Input(X+70, Y, 70, 24, "x: ");
	pos_y = new Fl_Int_Input(X+70, Y + 28, 70, 24, "y: ");
	pos_z = new Fl_Int_Input(X+70, Y + 28*2, 70, 24, "z: ");

	pos_x->align(FL_ALIGN_LEFT);
	pos_y->align(FL_ALIGN_LEFT);
	pos_z->align(FL_ALIGN_LEFT);

	pos_x->callback(x_callback, this);
	pos_y->callback(y_callback, this);
	pos_z->callback(z_callback, this);

	pos_x->when(FL_WHEN_RELEASE | FL_WHEN_ENTER_KEY);
	pos_y->when(FL_WHEN_RELEASE | FL_WHEN_ENTER_KEY);
	pos_z->when(FL_WHEN_RELEASE | FL_WHEN_ENTER_KEY);

	Y = Y + 105;

	angle = new Fl_Int_Input(X+70, Y, 64, 24, "Angle: ");
	angle->align(FL_ALIGN_LEFT);
	angle->callback(angle_callback, this);
	angle->when(FL_WHEN_RELEASE | FL_WHEN_ENTER_KEY);


	int ang_mx = X + W - 75;
	int ang_my = Y + 17;

	for (int i = 0 ; i < 8 ; i++)
	{
		int dist = (i == 2 || i == 6) ? 32 : 35;

		int x = ang_mx + dist * cos(i * 45 * M_PI / 180.0);
		int y = ang_my - dist * sin(i * 45 * M_PI / 180.0);

		ang_buts[i] = new Fl_Button(x - 9, y - 9, 24, 24, 0);

		ang_buts[i]->image(new Fl_Pixmap(arrow_pixmaps[i]));
		ang_buts[i]->align(FL_ALIGN_CENTER);
		ang_buts[i]->clear_visible_focus();
		ang_buts[i]->callback(button_callback, this);
	}
	
	end();

	resizable(NULL);
}

void UI_PathNodeBox::button_callback(Fl_Widget *w, void *data)
{
	UI_PathNodeBox *box = (UI_PathNodeBox *)data;

	// check for the angle buttons
	for (int i = 0 ; i < 8 ; i++)
	{
		if (w == box->ang_buts[i])
		{
			char buffer[20];

			sprintf(buffer, "%d", i * 45);

			box->angle->value(buffer);

			angle_callback(box->angle, box);
		}
	}
}

void UI_PathNodeBox::x_callback(Fl_Widget *w, void *data)
{
	UI_PathNodeBox *box = (UI_PathNodeBox *)data;

	int new_x = atoi(box->pos_x->value());

	selection_c list;
	selection_iterator_c it;

	if (GetCurrentObjects(&list))
	{
		BA_Begin();

		for (list.begin(&it); !it.at_end(); ++it)
			BA_ChangePN(*it, ZB_PathNode::F_X, new_x);

		BA_MessageForSel("edited X of", &list);
		BA_End();
	}
}

void UI_PathNodeBox::y_callback(Fl_Widget *w, void *data)
{
	UI_PathNodeBox *box = (UI_PathNodeBox *)data;

	int new_y = atoi(box->pos_y->value());

	selection_c list;
	selection_iterator_c it;

	if (GetCurrentObjects(&list))
	{
		BA_Begin();

		for (list.begin(&it); !it.at_end(); ++it)
			BA_ChangePN(*it, ZB_PathNode::F_Y, new_y);

		BA_MessageForSel("edited Y of", &list);
		BA_End();
	}
}

void UI_PathNodeBox::z_callback(Fl_Widget *w, void *data)
{
	UI_PathNodeBox *box = (UI_PathNodeBox *)data;

	int new_z = atoi(box->pos_z->value());

	selection_c list;
	selection_iterator_c it;

	if (GetCurrentObjects(&list))
	{
		BA_Begin();

		for (list.begin(&it); !it.at_end(); ++it)
			BA_ChangePN(*it, ZB_PathNode::F_Z, new_z);

		BA_MessageForSel("edited Z of", &list);
		BA_End();
	}
}

void UI_PathNodeBox::type_callback(Fl_Widget *w, void *data)
{
	UI_PathNodeBox *box = (UI_PathNodeBox *)data;
	Fl_Choice *mb = (Fl_Choice*) w;

	ZB_NavigationType new_type = ZB_NavigationType(mb->value());

	selection_c list;
	selection_iterator_c it;

	if (GetCurrentObjects(&list))
	{
		BA_Begin();

		for (list.begin(&it) ; !it.at_end() ; ++it)
		{
			BA_ChangePN(*it, ZB_PathNode::F_TYPE, new_type);
		}

		BA_MessageForSel("edited navigation type of", &list);
		BA_End();
	}
}

void UI_PathNodeBox::angle_callback(Fl_Widget *w, void *data)
{
	UI_PathNodeBox *box = (UI_PathNodeBox *)data;

	int new_ang = atoi(box->angle->value());

	selection_c list;
	selection_iterator_c it;

	if (GetCurrentObjects(&list))
	{
		BA_Begin();

		for (list.begin(&it); !it.at_end(); ++it)
		{
			BA_ChangePN(*it, ZB_PathNode::F_ANGLE, new_ang);
		}

		BA_MessageForSel("edited angle of", &list);
		BA_End();
	}
}

void UI_PathNodeBox::UpdateGameInfo() {
	// nothing to do here, really... I guess.
}

void UI_PathNodeBox::BrowsedItem(char kind, int number, const char *name, int e_state) {
	// here neither
}

void UI_PathNodeBox::UpdateTotal() {
	which->SetTotal(PathNodes.size());
}

void UI_PathNodeBox::SetObj(int _index, int _count)
{
	if (obj == _index && count == _count)
		return;

	obj   = _index;
	count = _count;

	which->SetIndex(obj);
	which->SetSelected(count);

	UpdateField();

	redraw();
}

#include <iostream>
void UI_PathNodeBox::UpdateField(int field)
{
	if (!is_pathnode(obj) || !PathNodes[obj]) {
		angle->value("");
		navType->value(0);
		pos_x->value("");
		pos_y->value("");
		pos_z->value("");

		return;
	}

	ZB_PathNode *nod = PathNodes[obj];

	if (((unsigned int) (nod->getAngle())) <= 32767)
		angle->value(Int_TmpStr(nod->getAngle()));

	else {
		std::stringstream ss;
		ss << "-" << 65536 - (unsigned int) nod->getAngle();
		angle->value(ss.str().c_str());
	}

	pos_x->value(Int_TmpStr(nod->getX()));
	pos_y->value(Int_TmpStr(nod->getY()));
	pos_z->value(Int_TmpStr(nod->getZ()));

	navType->value(nod->getType());
}

bool UI_PathNodeBox::ClipboardOp(char op)
{
	return false;
}

UI_PathNodeBox::~UI_PathNodeBox()
{
}