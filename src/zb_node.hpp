#pragma once

#include <string>
#include <vector>



enum ZB_NavigationType {
    NT_NORMAL = 0,
    NT_USE,
    NT_SLOW,
    NT_CROUCH,
    NT_JUMP,
    NT_AVOID,
    NT_SHOOT,
    NT_RESPAWN,
    NT_TARGET
};

#define NTCOUNT 8


class ZB_PathNode {
    int x, y, z;
    ZB_NavigationType nodeType;
    int angle;

public:
    ZB_PathNode();
    ZB_PathNode(int x, int y, int z, ZB_NavigationType nodeType, unsigned short angle);
    ZB_PathNode(std::string commaData);
    
    std::string getCommaData();

    void offset(int xOff, int yOff);
    void move(int x, int y);
    void setHeight(int z);
    void turnTo(unsigned short angle);
    void setNavigationType(ZB_NavigationType newNT);
    void Parse(std::string commaData);

    int getX();
    int getY();
    int getZ();
    ZB_NavigationType getType();
    unsigned short getAngle();

    enum { F_X, F_Y, F_Z, F_TYPE, F_ANGLE };
};


class ZB_MapNodes {
public:
    std::string mapName;
    std::vector<ZB_PathNode*> mapNodes;
    
    ZB_MapNodes();
    ZB_MapNodes(std::string mapName); // empty
    
    std::string getData();
};

void ZB_ParseNodes(std::string data);