cmake_minimum_required(VERSION 3.1)
project(eureka)

cmake_policy(SET CMP0072 NEW)


add_subdirectory("deps")

include_directories("src")

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")

add_subdirectory("src")